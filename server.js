console.log("Aqui funcionando, con nodemon")

var movimientosJSON = require('./movimientosv2.json')
var usuariosJSON = require('./usuarios.json')
var express = require('express')

var bodyparser = require('body-parser')
//var jsonQuery = require('json-query')
var requestJson = require('request-json')
var fs = require('fs')

var app = express()
app.use(bodyparser.json())

app.get('/', function(req, res)
{
  res.send('Hola API')
})

app.get('/v1/movimientos', function(req, res){
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos', function(req, res){
  res.send(movimientosJSON)
  //res.sendfile('movimientosv2.json')
})

app.get('/v2/movimientosq', function(req, res){
  console.log(req.query);
  //res.sendfile('movimientosv2.json')
  res.send("recibido")
})

app.get('/v2/movimientos/:id', function(req, res){
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  res.send(movimientosJSON[req.params.id-1])
  //res.send("Hemos recibido su peticiòn de consulta del movimiento #" + req.params.id)
  //res.sendfile('movimientosv2.json')
})

app.post('/v2/movimientos/', function(req, res){
  //console.log(req.headers['authorization'])
  if (req.headers['authorization']==="abc") {
    var nuevo = req.body
    nuevo.id = movimientosJSON.length + 1
    movimientosJSON.push(req.body)
    res.send("Movimiento dado de alta")
  }else {
    res.send("No está autorizado")
  }

})


app.put('/v2/movimientos/:id', function(req, res){
  var cambios = req.body
  var actual = movimientosJSON[req.params.id-1]
  if (cambios.importe != undefined)
  {    actual.importe = cambios.importe}
  if (cambios.ciudad != undefined)
  {    actual.ciudad = cambios.ciudad}
  if (cambios.fecha != undefined)
  {    actual.fecha = cambios.fecha}
  if (cambios.id != undefined)
  {    actual.id = cambios.id}
  res.send("Movimiento modificado")
})

app.delete('/v2/movimientos/:id', function(req, res){
  //console.log(req)
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })
  res.send("Movimiento anulado")
})

//GET USUARIOS
app.get('/v2/usuarios', function(req, res){
  res.send(usuariosJSON)
})

//POST Login
app.post('/v2/usuarios/', function(req, res){
  //console.log(req.headers['pass'])
  var usuarioCoincide
  correo=req.headers['email']

  for (var i = 0; i < usuariosJSON.length; i++) {
    if (usuariosJSON[i].email==correo) {
      usuarioCoincide = usuariosJSON[i]
    }
  }
  if (usuarioCoincide != undefined) {
  var pass = req.headers['pass']
    if (usuarioCoincide.pass == pass) {
      usuarioCoincide.estado = "logueado"
      res.send("Logueado!!")
    }else {
        res.send("Datos incorrectos")
    }
  }else {
  res.send("Introducir usuario válido")
  }
})

//POST Login
app.post('/v2/usuarios/login', function(req, res) {
  var email = req.headers['email']
  var password = req.headers['password']
  var resultados = jsonQuery('[email=' + email + ']', {data:usuariosJSON})
  if (resultados.value != null && resultados.value.password == password) {
    usuariosJSON[resultados.value.id-1].estado='logged'
    res.send('{"login:"OK"}')
  }
  else {
    res.send('{"login":"error"}')
  }
})

//POST Logout

app.post('/v2/usuarios/logout/:id', function(req, res){
  //console.log(req.headers['pass'])
  var usuarioCoincide = usuariosJSON[req.params.id-1]
  if (usuarioCoincide != undefined) {
    usuarioCoincide.estado = "deslogueado"
    res.send("Usuario deslogueado")
  }else {
  res.send("Introducir usuario válido")
  }
})

//Version 3 de la API conectada a MLab

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/imptechumx/collections/"
var apiKey = "apiKey=LLSj20EHfe7aubO7wlLu5CXYvgxPtmrb"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)

app.get('/v3', function(req, res) {
    clienteMlab.get('', function(err, resM, body){
        var coleccionesUsuario = []
        if (!err) {
          for (var i = 0; i < body.length; i++) {
            if (body[i]!="system.indexes") {
              coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
            }
          }
            res.send(coleccionesUsuario)
        }else {
          res.send(err)
        }

    })
})

app.get('/v3/usuarios', function(req, res) {
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get('', function(err, resM, body){
      res.send(body)
  })
})

app.get('/v3/usuarios/:id', function(req, res) {
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
    clienteMlab.get('?q={"id":'+ req.params.id + '}&' + apiKey,
    function(err, resM, body){
      res.send(body)
  })
})

app.post('/v3/usuarios', function(req, res){
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})

// Se hace el PUT facilitando que no se utilice el {	"$set":______}
app.put('/v3/usuarios/:id', function(req, res){
    var clienteMlab = requestJson.createClient(urlMlabRaiz + "/otherfiles/wallpaper.pdf")
    var cambio = '{	"$set":'+ JSON.stringify(req.body) +'}'
        clienteMlab.put('?q={"id":'+ req.params.id + '}&' + apiKey, JSON.parse(cambio),
        function(err, resM, body){
          res.send(body)
          //console.log('El body es'+req.body.country);
          //req.body('{	"$set":'+body+'}')
      })
})

app.get('/v4', function(req, res){
  var wallpaperPDF = "./otherfiles/wallpaper.pdf";

fs.readFile(wallpaperPDF, function (err, data){
  res.contentType("application/pdf");
  res.send(data)
});
})

app.listen(3000)
console.log("Escuchando en el puerto 3000")
